package com.yinxin.shiroRealm.tools;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.codec.Hex;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 编码工具类
 * @date 2022/5/3010:12
 */
public class EncodesUtil {
    /**
     * @Description HEX-byte[]-string
     * @param input 输入数组
     * @return 字符串
     */
    public static String encodeHex(byte[] input){
        return Hex.encodeToString(input);
    }

    /**
     * @Description HEX-string-byte[]
     * @param input 输入字符串
     * @return byte数组
     */
    public static byte[] decodeHex(String input){
        return Hex.decode(input);
    }

    /**
     * @Description Base64-byte[]-string
     * @param input 输入byte数组
     * @return 字符串
     */
    public static String encodeBase64(byte[] input){
        return Base64.encodeToString(input);
    }

    /**
     * @Description Base64-string-byte[]
     * @param input 输入字符串
     * @return byte数组
     */
    public static byte[] decodeBase64(String input){
        return Base64.decode(input);
    }
}
