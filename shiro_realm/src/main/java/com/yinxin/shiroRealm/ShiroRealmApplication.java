package com.yinxin.shiroRealm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShiroRealmApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroRealmApplication.class, args);
    }

}
