package com.yinxin.shiroRealm.service.impl;

import com.yinxin.shiroRealm.service.SecurityService;
import com.yinxin.shiroRealm.tools.DigestsUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2710:39
 */
@Service
public class SecurityServiceImpl implements SecurityService {

    /**
     * 权限服务层
     * @param loginName 登录名
     * @return
     */
    @Override
    public Map<String,String> findPasswordByLoginName(String loginName) {
        return DigestsUtil.entryptPassword("123");
    }

    @Override
    public List<String> findRoleByLoginName(String loginName) {
        List<String> list = new ArrayList<>();
        list.add("admin");
        list.add("dev");
        return list;
    }

    @Override
    public List<String> findPermissionByLoginName(String loginName) {
        List<String> list= new ArrayList<>();
        list.add("order:add");
        list.add("order:list");
        list.add("order:del");
        list.add("order:update");
        return list;
    }
}
