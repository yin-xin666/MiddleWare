package com.yinxin.shiroRealm.service;

import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2710:39
 */
public interface SecurityService {

    /**
     * 查找用户密码
     * @param loginName 登录名
     * @return 密码和盐的map
     */
    Map<String,String> findPasswordByLoginName(String loginName);

    /**
     * 查询角色
     * @param loginName 用户名
     * @return 角色字符串列表
     */
    List<String> findRoleByLoginName(String loginName);

    /**
     * 查询资源
     * @param loginName 用户名
     * @return 资源字符串列表
     */
    List<String> findPermissionByLoginName(String loginName);
}
