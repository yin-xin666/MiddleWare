package com.yinxin.rabbitmq.one;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author yinxin
 * @Title: rabbitmq
 * @Package com.yinxin
 * @Description: rabbitmq学习案例
 * @date 2022/2/1315:23
 */

public class Producer {

    public static final String QUEUE_NAME = "hello";

    //发消息
    public static void main(String[] args) throws IOException, TimeoutException {
        //创建一个连接工厂
        ConnectionFactory factory=new ConnectionFactory();
        //工厂IP 连接RabbitMQ的队列，设置用户名、密码
        factory.setHost("192.168.200.129");
        factory.setUsername("guest");
        factory.setPassword("guest");

        //创建连接,获取连接信道
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        /**
         * 生成一个队列
         *1、队列名称
         *2、队列里面的消息是否持久化（磁盘） 默认情况消息存储在内存中
         * 3、该队列是否只供一个消费者消费 是否进行消息共享，true可以多个消费者消费 false：只能一个消费者消费
         * 4、是否自动化删除 最后一个消费者端开连接以后 该队一个是否自动删除 true自动删除 false不自动删除
         * 5、其他参数
         */
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        //发消息
        String message = "hello world";//初次使用
        /**
         * 发送一个消息
         * 1、发送到那个交换机
         * 2、路由的key值是哪个 本次是队列的名称
         * 3、其他参数
         * 4、发送消息的消息体
         */
        channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
        System.out.println("消息发送完毕");
    }
}
