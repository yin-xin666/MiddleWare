package com.yinxin.rabbitmq.one;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author yinxin
 * @Title: rabbitmq
 * @Package com.yinxin
 * @Description: rabbitmq学习案例
 * @date 2022/2/1315:23
 */
public class Consumer {

    public static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException, TimeoutException {

        //创建一个连接工厂==》工厂IP 连接RabbitMQ的队列，设置用户名、密码==》创建连接,获取连接信道
        ConnectionFactory factory=new ConnectionFactory();

        factory.setHost("192.168.200.129");
        factory.setUsername("guest");
        factory.setPassword("administrator");

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        //声明
        DeliverCallback deliverCallback=(consumerTag,message)->{
            System.out.println(new String(message.getBody()));
        };

        /**
         * 消费者消费消息
         * 1、消费哪个队列
         * 2、消费成功之后是否需要自动应答 true 代表的自动应答 false 代表手动应答
         * 3、消费者未成功消费的回调
         * 4、消费者取消消费回调
         */

    }
}
