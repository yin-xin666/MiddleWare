package com.yinxin.kafka;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/815:15
 */
public class KafkaConsumerTest {

//    public static String TOPIC_NAME = "xdclass-partion5";

    public static Properties getProperties() {
        Properties props = new Properties();
        //broker地址
        props.put("bootstrap.servers", "192.168.200.200:9092");
        //消费者分组ID，分组内的消费者只能消费该消息一次，不同分组内的消费者可以重复消费该消息
        props.put("group.id", "group-1");
        //设置消费起点
        props.put("auto.offset.reset","earliest");
        //开启自动提交offset或者手动提交
//        props.put("enable.auto.commit", "true");
        props.put("enable.auto.commit", "false");
        //自动提交offset延迟时间
//        props.put("auto.commit.interval.ms", "1000");
        //反序列化
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        return props;
    }

    @Test
    public void simpleConsumerTest() {
        Properties properties = getProperties();

        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties);
        // 一、订阅主题
//        kafkaConsumer.subscribe(Arrays.asList(KafkaProducerTest.TOPIC_NAME));

        //二、指定pation消费
        // 获取topic的partition信息，给Consumer指定消费的Topic和Partition(十分重要)
        TopicPartition topicPartition = new TopicPartition(KafkaProducerTest.TOPIC_NAME, 0);
        // 如果无对应topic或者partition,则会抛出异常IllegalArgumentException
        // 如果此consumer之前已经有过订阅行为且未解除之前所有的订阅,则会抛出异常IllegalStateException
        kafkaConsumer.assign(Collections.singletonList(topicPartition));
        // 6.覆盖原始的Consumer-Topic-Partition对应的Offset,将其设置为指定Offset值
        //kafkaConsumer.seek(topicPartition, 39);

        while (true) {
            ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record : records) {
                System.err.printf("topic=%s,offset=%d,key=%s,value=%s %n", record.topic(), record.offset(), record.key(), record.value());
            }
            //同步阻塞提交offset
//            kafkaConsumer.commitSync();
            if (!records.isEmpty()) {
                //异步提交offset
                kafkaConsumer.commitAsync(new OffsetCommitCallback() {
                    @Override
                    public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, Exception exception) {
                        if (exception == null) {
                            System.out.println("手工提交offset成功" + offsets.toString());
                        } else {
                            System.out.println("手工提交offset失败" + offsets.toString());
                        }
                    }
                });
            }
        }
    }

    @Test
    public void ConsumerTest(){
        Properties properties = getProperties();
        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(Arrays.asList(KafkaProducerTest.TOPIC_NAME));
    }


}
