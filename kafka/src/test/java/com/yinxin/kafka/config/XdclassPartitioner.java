package com.yinxin.kafka.config;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.utils.Utils;

import java.security.Key;
import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description: kafka
 * @date 2022/3/811:47
 */
public class XdclassPartitioner implements Partitioner {

    @Override
    public int partition(String topic, Object key, byte[] keybytes, Object value, byte[] valuebytes, Cluster cluster) {

        if(keybytes==null){
            throw new IllegalArgumentException("参数不能为空");
        }

        if("xdclass".equals(key)){
            return 0;
        }
        List<PartitionInfo> partitionInfos = cluster.partitionsForTopic(topic);
        int size = partitionInfos.size();
        return Utils.toPositive(Utils.murmur2(keybytes))%size;
    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}
