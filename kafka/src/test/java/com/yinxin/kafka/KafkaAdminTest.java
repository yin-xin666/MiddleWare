package com.yinxin.kafka;

import org.apache.kafka.clients.admin.*;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * @author yinxin
 * @version 1.0
 * @Description: kafka操作的工具类
 * @date 2022/3/212:47
 */
public class KafkaAdminTest {
    private static final String TOPIC_NAME="xdclass-partion5";
    /**
     *  设置Admin 客户端
     * @return
     */
    public static AdminClient initAdminClient(){
        Properties properties = new Properties();
        properties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.200.200:9092");;
        AdminClient adminClient = AdminClient.create(properties);
        return adminClient;
    }

    /**
     * 创建topic
     */
    @Test
    public void createTopicTest(){
        AdminClient adminClient=initAdminClient();

        //指定分区，副本数量xdclass-t3
        NewTopic newTopic = new NewTopic(TOPIC_NAME,5,(short)1);
        CreateTopicsResult createTopicsResult = adminClient.createTopics(Arrays.asList(newTopic));
        try {
            //future等待创建，成功则不会有任何报错
            createTopicsResult.all().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("创建新的topic");
    }

    /**
     * 列举当前的topic
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void listTopicTest() throws ExecutionException, InterruptedException {
        AdminClient adminClient=initAdminClient();
        //是否查看内部的topic、可以不用
        ListTopicsOptions options = new ListTopicsOptions();
        options.listInternal(true);

        //打印topic
        ListTopicsResult listTopicsResult = adminClient.listTopics();

        Set<String> topics = listTopicsResult.names().get();
        for (String topic : topics) {
            System.err.println(topic);
        }
    }

    /**
     * 删除topic
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void deleteTopic() throws ExecutionException, InterruptedException {
        AdminClient adminClient=initAdminClient();
        DeleteTopicsResult deleteTopicsResult = adminClient.deleteTopics(Arrays.asList("xdclass-t3"));
        System.out.println(deleteTopicsResult.all().get());
    }

    /**
     * 查看topic详情
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    public void detailTopicTest() throws ExecutionException, InterruptedException {
        AdminClient adminClient=initAdminClient();
        DescribeTopicsResult describeTopicsResult = adminClient.describeTopics(Arrays.asList(TOPIC_NAME));
        Map<String, TopicDescription> stringTopicDescriptionMap = describeTopicsResult.all().get();
        Set<Map.Entry<String, TopicDescription>> entries = stringTopicDescriptionMap.entrySet();
        entries.stream().forEach((entry)-> System.err.println("name:"+entry.getKey()+",desc:"+entry.getValue()));
    }

    /**
     * 增加分区数量
     * 如果当主题中的消息包含有key时(即key不为null)，根据key来计算分区的行为就会有所影响消息顺序性
     * 注意：Kafka中的分区数只能增加不能减少，减少的话数据不知怎么处理
     *
     * @throws Exception
     */

    @Test
    public  void incrPartitionsTest() throws Exception{
        Map<String, NewPartitions> infoMap = new HashMap<>();
        NewPartitions newPartitions = NewPartitions.increaseTo(3);
        AdminClient adminClient = initAdminClient();
        infoMap.put(TOPIC_NAME, newPartitions);
        CreatePartitionsResult createPartitionsResult = adminClient.createPartitions(infoMap);
        createPartitionsResult.all().get();
    }
}
