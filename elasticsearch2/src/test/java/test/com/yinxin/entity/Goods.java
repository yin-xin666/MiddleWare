package test.com.yinxin.entity;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/1516:11
 */
public class Goods {
    private String brandName;
    private String title;
    private String categoryName;
    private Double price;

    public Goods() {
    }

    public Goods(String brandName, String title, String categoryName, Double price) {
        this.brandName = brandName;
        this.title = title;
        this.categoryName = categoryName;
        this.price = price;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "brandName='" + brandName + '\'' +
                ", title='" + title + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", price=" + price +
                '}';
    }
}
