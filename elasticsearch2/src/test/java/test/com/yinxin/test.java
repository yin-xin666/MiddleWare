package test.com.yinxin;


import com.alibaba.fastjson.JSON;
import com.yinxin.ElasticsearchApp;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.*;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import test.com.yinxin.entity.Goods;

import java.io.IOException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElasticsearchApp.class)
public class test {


    @Autowired
    private RestHighLevelClient restHighLevelClient;


    private IndicesClient indices;

//==============索引的创建=================
    @Before
    public void before(){
        indices = restHighLevelClient.indices();
    }
    /**
     * 创建索引
     */
    @Test
    public void createIndex1() throws IOException {
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("goods");
        CreateIndexResponse createIndexResponse = indices.create(createIndexRequest, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse.isAcknowledged());
    }
    /**
     * 删除索引
     * @throws IOException
     */
    @Test
    public void deleteIndex() throws IOException {
        DeleteIndexRequest deleteRequest = new DeleteIndexRequest("itcast");
        AcknowledgedResponse response = indices.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println(response.isAcknowledged());
    }

    /**
     * 判断索引是否存在
     * @throws IOException
     */
    @Test
    public void existIndex() throws IOException {
        IndicesClient indices = restHighLevelClient.indices();
        GetIndexRequest getIndexRequest = new GetIndexRequest("goods");
        boolean exists = indices.exists(getIndexRequest, RequestOptions.DEFAULT);
        System.out.println(exists);

    }

    /**
     * 创建索引并添加mapping
     * @throws IOException
     */
    @Test
    public void createIndex2() throws IOException {
        IndicesClient indices = restHighLevelClient.indices();
        CreateIndexRequest createIndexRequest = new CreateIndexRequest("goods");
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        {
            builder.startObject("mappings");
            {
                builder.startObject("properties");
                {
                    builder.startObject("categoryName");
                    {
                        builder.field("type", "text");
                    }
                    builder.endObject();
                    builder.startObject("brandName");
                    {
                        builder.field("type", "keyword");
                    }
                    builder.endObject();
                    builder.startObject("title");
                    {
                        builder.field("type", "keyword");
                    }
                    builder.endObject();
                    builder.startObject("price");
                    {
                        builder.field("type", "double");
                    }
                    builder.endObject();
                }
                builder.endObject();
            }
            builder.endObject();
        }
        builder.endObject();
        createIndexRequest.source(builder);
        CreateIndexResponse createIndexResponse = indices.create(createIndexRequest, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse.isAcknowledged());
    }


//==============操作文档===============
    /**
     * 添加    文档，使用map作为数据
     * @throws IOException
     */
    @Test
    public void addDoc() throws IOException {
        //数据对象，map
        Map data = new HashMap();
        data.put("categoryName","笔记本呢");
        data.put("brandName","苹果");
        data.put("title","电子产品");
        data.put("price",10000.15);
        IndexRequest indexRequest = new IndexRequest("goods").id("9").source(data);
        IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(response.getId());
    }


    /**
     * 添加    文档，使用对象作为数据
     * @throws IOException
     */
    @Test
    public void addDoc2() throws IOException {
        //数据对象
        Doc doc = new Doc("4",222L, "内容2是这个些4", 23.23);
        String docStr = JSON.toJSONString(doc);
        IndexRequest indexRequest = new IndexRequest("itcast").id(doc.getId()).source(docStr,XContentType.JSON);
        IndexResponse response = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        System.out.println(response.getId());
    }

    /**
     * 根据id查询文档
     */
    @Test
    public void findDocById() throws IOException {
        GetRequest getReqeust = new GetRequest("itcast","2");
        //getReqeust.id("1");
        GetResponse response = restHighLevelClient.get(getReqeust, RequestOptions.DEFAULT);
        //获取数据对应的json
        System.out.println(response.getSourceAsString());
    }

    /**
     *查看
     */
    @Test
    public void getIndex() throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest("itcast");
        GetIndexResponse getIndexResponse = indices.get(getIndexRequest, RequestOptions.DEFAULT);
        System.out.println(getIndexResponse);
        Map<String, MappingMetaData> mappings = getIndexResponse.getMappings();
        for (Map.Entry<String, MappingMetaData> stringMappingMetaDataEntry : mappings.entrySet()) {
            System.out.println(mappings.get(stringMappingMetaDataEntry.getKey()));
        }
    }

    /**
     * 根据id删除文档
     */
    @Test
    public void delDoc() throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest("itcast","1");
        DeleteResponse response = restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
        System.out.println(response.getId());
    }

//==============批处理文件===============
    /**
     * 1. 批量操作 bulk
     */
    @Test
    public void testBulk() throws IOException{
        //创建request对像整合所有操作
        BulkRequest bulkRequest = new BulkRequest();

        /*
        # 1. 删除2号记录
        # 2. 添加1号记录
        # 3. 修改3号记录 名称为 “三号”
         */
        //添加对应操作
        //1. 删除2号记录
        DeleteRequest deleteRequest = new DeleteRequest("itcast","2");
        bulkRequest.add(deleteRequest);

        //2. 添加1号记录
        Map map = new HashMap();
        map.put("content","good insert");
        IndexRequest indexRequest = new IndexRequest("itcast").id("1").source(map);
        bulkRequest.add(indexRequest);

        //3. 修改3号记录 名称为 “三号”
        Map map2 = new HashMap();
        map2.put("content","三号记录");
        UpdateRequest updateReqeust = new UpdateRequest("itcast","3").doc(map2);
        bulkRequest.add(updateReqeust);

        //执行批量操作
        BulkResponse response = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        for (BulkItemResponse item : response.getItems()) {
            System.out.println(item.status());
        }
    }

//==============各种查询===============

    /**
     * 查询所有
     *  1. matchAll
     *  2. 将查询结果封装为Doc对象，装载到List中
     *  3. 分页。默认显示2条
     */
    @Test
    public void testMatchAll() throws IOException {
        //2. 构建查询请求对象，指定查询的索引名称
        SearchRequest searchRequest = new SearchRequest("nba");
        //4. 创建查询条件构建器SearchSourceBuilder
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        //6. 查询条件
        QueryBuilder query = QueryBuilders.matchAllQuery();//查询所有文档
        //5. 指定查询条件
        sourceBuilder.query(query);
        //3. 添加查询条件构建器 SearchSourceBuilder
        searchRequest.source(sourceBuilder);
        // 8 . 添加分页信息
        sourceBuilder.from(0);
        sourceBuilder.size(2);
        //1. 查询,获取查询结果
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        //7. 获取命中对象 SearchHits
        SearchHits searchHits = searchResponse.getHits();
        //7.1 获取总记录数
        long value = searchHits.getTotalHits().value;
        System.out.println("总记录数："+value);
        List<Doc> docList = new ArrayList<>();
        //7.2 获取Hits数据  数组
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            //获取json字符串格式的数据
            String sourceAsString = hit.getSourceAsString();
            //转为java对象
            Doc doc = JSON.parseObject(sourceAsString, Doc.class);
            docList.add(doc);
        }

        for (Doc doc : docList) {
            System.out.println(doc);
        }
    }

    //输出es搜索出来的词条的各个结果
    private void outResponse(SearchResponse searchResponse){
        SearchHits searchHits = searchResponse.getHits();
        long value = searchHits.getTotalHits().value;
        System.err.println("查询出来总数为:"+value);
        SearchHit[] hits = searchHits.getHits();
        for (SearchHit hit : hits) {
            System.err.println(hit);
        }
    }
    /**
     * termQuery:词条查询
     */
    @Test
    public void testTermQuery() throws IOException{
        SearchRequest searchRequest = new SearchRequest("itcast");
        SearchSourceBuilder SourceBuilder = new SearchSourceBuilder();
        TermQueryBuilder query = QueryBuilders.termQuery("agscoree", 23.23);//term词条查询
        SourceBuilder.query(query);
        searchRequest.source(SourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }
    /**
     * matchQuery:词条分词查询
     */
    @Test
    public void testMatchQuery() throws IOException{
        SearchRequest searchRequest = new SearchRequest("yinxin");
        SearchSourceBuilder sourceBulider = new SearchSourceBuilder();
        MatchQueryBuilder query = QueryBuilders.matchQuery("firstCode", "电器");
        query.operator(Operator.AND);//求并集
        sourceBulider.query(query);
        searchRequest.source(sourceBulider);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }
    /**
     * 查找指定字段在指定范围内包含值
     * 1. 范围查询：rangeQuery
     * 2. 排序
     */
    @Test
    public void testRangeQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("yinxin");

        SearchSourceBuilder sourceBulider = new SearchSourceBuilder();
        //范围查询
        RangeQueryBuilder query = QueryBuilders.rangeQuery("id");
        //指定下限
        query.gte(1);
        //指定上限
        query.lte(5);
        sourceBulider.query(query);
        //排序
        sourceBulider.sort("id", SortOrder.ASC);
        searchRequest.source(sourceBulider);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }

    /**
     * queryString
     * 会对查询条件进行分词。
     * 然后将分词后的查询条件和词条进行等值匹配
     * 默认取并集（OR）
     * 可以指定多个查询字段
     */
    @Test
    public void testQueryStringQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("yinxin");
        SearchSourceBuilder sourchBuilder = new SearchSourceBuilder();
        QueryStringQueryBuilder queryBuilder = QueryBuilders.queryStringQuery("熨斗 桌子")
                .field("secordCode").
                field("content");
        sourchBuilder.query(queryBuilder);
        searchRequest.source(sourchBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }

    /**
     * 布尔查询
     * 对多个查询条件连接
     * @throws IOException
     */
    @Test
    public void testBoolQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("goods");
        SearchSourceBuilder sourchBuilder = new SearchSourceBuilder();
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        QueryBuilder tremQueryBuilder = QueryBuilders.termQuery("brandName","华为");
        queryBuilder.must(tremQueryBuilder);
        QueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("brandName","为");
        queryBuilder.should(matchQueryBuilder);
        QueryBuilder filterTremQueryBuilder = QueryBuilders.termQuery("brandName","华为");
        queryBuilder.filter(filterTremQueryBuilder);
        sourchBuilder.query(queryBuilder);
        searchRequest.source(sourchBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }
    /**
     * 聚合查询：桶聚合，分组查询
     * 1. 查询title包含手机的数据
     * 2. 查询品牌列表
     */
    @Test
    public void testAggQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("goods");
        SearchSourceBuilder sourceBulider = new SearchSourceBuilder();
        // 1. 查询title包含手机的数据
        MatchQueryBuilder query = QueryBuilders.matchQuery("title", "电子产品");
        sourceBulider.query(query);
        // 2. 查询品牌列表
    //参数：
        //1. 自定义的名称，将来用于获取数据
        //2. 分组的字段
        AggregationBuilder agg = AggregationBuilders.terms("goods_brands").field("brandName").size(4);
        sourceBulider.aggregation(agg);
        searchRequest.source(sourceBulider);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
        // 获取桶聚合结果
        Aggregations aggregations = searchResponse.getAggregations();
        Map<String, Aggregation> aggregationMap = aggregations.asMap();
//        System.out.println(aggregationMap); //{goods_brands=org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms@76ad6715}
        Terms goods_brands = (Terms) aggregationMap.get("goods_brands");
        List<? extends Terms.Bucket> buckets = goods_brands.getBuckets();
        List brands = new ArrayList();
        for (Terms.Bucket bucket : buckets) {
            Object key = bucket.getKey();
            brands.add(key);
        }
        for (Object brand : brands) {
            System.out.println(brand);
        }
    }


    /**
     *
     * 高亮查询：
     *  1. 设置高亮
     *      * 高亮字段
     *      * 前缀
     *      * 后缀
     *  2. 将高亮了的字段数据，替换原有数据
     */
    @Test
    public void testHighLightQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("goods");
        SearchSourceBuilder sourceBulider = new SearchSourceBuilder();
        // 1. 查询categoryName包含空气的数据
        MatchQueryBuilder query = QueryBuilders.matchQuery("categoryName", "手机");
        sourceBulider.query(query);
        //设置高亮
        HighlightBuilder highlighter = new HighlightBuilder();
        //设置三要素
        highlighter.field("categoryName");
        highlighter.preTags("<font color='red'>");
        highlighter.postTags("</font>");
        sourceBulider.highlighter(highlighter);

        // 2. 查询品牌列表
    //参数：
        //1. 自定义的名称，将来用于获取数据
        //2. 分组的字段
        AggregationBuilder agg = AggregationBuilders.terms("goods_brands").field("brandName").size(4);
        sourceBulider.aggregation(agg);
        searchRequest.source(sourceBulider);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
//        outResponse(searchResponse);
        SearchHits searchHits = searchResponse.getHits();
        //获取记录数
        long value = searchHits.getTotalHits().value;
        System.out.println("总记录数："+value);
        SearchHit[] hits = searchHits.getHits();

        List<Goods> goodsList = new ArrayList<>();
        for (SearchHit hit : hits) {
            String sourceAsString = hit.getSourceAsString();
            Goods goods = JSON.parseObject(sourceAsString, Goods.class);
            // 获取高亮结果，数据中的categoryName
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField HighlightField = highlightFields.get("categoryName");
            Text[] fragments = HighlightField.fragments();
            goods.setCategoryName(fragments[0].toString());
            goodsList.add(goods);
        }
        System.out.println(goodsList);


        // 获取聚合结果
        Aggregations aggregations = searchResponse.getAggregations();
        Map<String, Aggregation> aggregationMap = aggregations.asMap();
        Terms goods_brands = (Terms) aggregationMap.get("goods_brands");
        List<? extends Terms.Bucket> buckets = goods_brands.getBuckets();
        for (Terms.Bucket bucket : buckets) {
            String keyAsString = bucket.getKeyAsString();
            System.out.println(keyAsString);
        }

    }



    /**
     * 模糊查询:WildcardQuery
     */
    @Test
    public void testWildcardQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("goods");
        SearchSourceBuilder sourceBulider = new SearchSourceBuilder();
        WildcardQueryBuilder query = QueryBuilders.wildcardQuery("brandName", "苹*");
        sourceBulider.query(query);
        searchRequest.source(sourceBulider);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }


    /**
     * 模糊查询:regexpQuery
     */
    @Test
    public void testRegexpQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("goods");
        SearchSourceBuilder sourceBulider = new SearchSourceBuilder();
        RegexpQueryBuilder query = QueryBuilders.regexpQuery("brandName", "荣(.)*");
        sourceBulider.query(query);
        searchRequest.source(sourceBulider);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }

    /**
     * 模糊查询:perfixQuery
     */
    @Test
    public void testPrefixQuery() throws IOException {
        SearchRequest searchRequest = new SearchRequest("goods");
        SearchSourceBuilder sourceBulider = new SearchSourceBuilder();
        PrefixQueryBuilder query = QueryBuilders.prefixQuery("brandName", "华");
        sourceBulider.query(query);
        searchRequest.source(sourceBulider);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        outResponse(searchResponse);
    }

}
