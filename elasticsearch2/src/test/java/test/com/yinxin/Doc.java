package test.com.yinxin;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/1317:12
 */
public class Doc {
    private String id;
    private Long title;
    private String content;
    private Double agscoree;

    public Doc() {
    }

    public Doc(String id, Long title, String content, Double agscoree) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.agscoree = agscoree;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTitle() {
        return title;
    }

    public void setTitle(Long title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getAgscoree() {
        return agscoree;
    }

    public void setAgscoree(Double agscoree) {
        this.agscoree = agscoree;
    }

    @Override
    public String toString() {
        return "Doc{" +
                "id='" + id + '\'' +
                ", title=" + title +
                ", content='" + content + '\'' +
                ", agscoree=" + agscoree +
                '}';
    }
}
