package com.yinxin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/1315:23
 */
@SpringBootApplication
public class ElasticsearchApp {
    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchApp.class,args);
    }
}
