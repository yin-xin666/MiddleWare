package com.yinxin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/311:11
 */
@SpringBootApplication
public class RedisApplication {
   public static void main(String[] args) {
      SpringApplication.run(RedisApplication.class,args);
   }
}
