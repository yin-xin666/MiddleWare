package com.yinxin.until;

import lombok.Data;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2610:46
 */
@Data
public class JsonData {

    /**
     * 状态码
     */
    private Integer code;
    /**
     * 数据
     */
    private Object data;
    /**
     * 描述
     */
    private String desc;

    public JsonData(Integer code, Object data, String desc) {
        this.code = code;
        this.data = data;
        this.desc = desc;
    }

    /**
     * 成功不穿参
     * @return
     */
    public static JsonData buildSuccess(){
        return new JsonData(200,null,null);
    }

    /**
     * 成功穿参
     * @return
     */
    public static JsonData buildSuccess(Object data,String desc){
        return new JsonData(200,data,desc);
    }

    /**
     * 失败不穿参
     * @return
     */
    public static JsonData buildFail(){
        return new JsonData(500,null,null);
    }

}

