package com.yinxin.service;

import com.yinxin.until.JsonData;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2610:55
 */
public interface CatcheService {
    JsonData getInfoWithCache(Integer id);

    JsonData hotCache(Integer id);

    JsonData getInfoNoCache(Integer id);
}
