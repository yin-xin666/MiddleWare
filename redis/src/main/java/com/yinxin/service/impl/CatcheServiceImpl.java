package com.yinxin.service.impl;

import com.alibaba.fastjson2.JSON;
import com.yinxin.dao.UserDao;
import com.yinxin.model.User;
import com.yinxin.service.CatcheService;
import com.yinxin.until.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2610:55
 */
@Service
public class CatcheServiceImpl implements CatcheService {

    @Autowired
    UserDao userDao;

    @Autowired
    RedisTemplate redisTemplate;



    @Override
    public JsonData getInfoWithCache(Integer id) {
        User user;
        String json = redisTemplate.boundValueOps(id).get().toString();
        user = JSON.parseObject(json, User.class);
        if(user==null){
            user = userDao.getOne(id);
        }
        return JsonData.buildSuccess(user,"访问成功！");
    }

    @Override
    public JsonData hotCache(Integer id) {
        User user = userDao.getOne(id);
        redisTemplate.boundValueOps(id).set(JSON.toJSONString(user));
        return JsonData.buildSuccess(null,"缓存成功");
    }

    @Override
    public JsonData getInfoNoCache(Integer id) {
        User user = userDao.getOne(id);
        return JsonData.buildSuccess(user,"访问成功！");
    }
}
