package com.yinxin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/311:12
 */
@RestController
public class Tetst {

    @Autowired
    StringRedisTemplate redisTemplate;

    @GetMapping("/test")
    public String test(){
        System.out.println(redisTemplate.boundValueOps("yinxin").get());
        //存在超时时间的设置
        redisTemplate.opsForValue().setIfAbsent("keyTimeOut","timeout",30, TimeUnit.SECONDS);

        return "success";
    }

    @GetMapping("/getTimes")
    public String getTimes(String key){
        Boolean aBoolean = invokeExceededTimes(key, 1, 4);
        if(aBoolean){
            return "访问成功!";
        }else {
            return "访问次数达到上限!";
        }
    }

    /**
     * redis实现限流访问呢
     * @param key 加锁key
     * @param num 锁时间
     * @param count 限制数量
     * @return
     */
    private Boolean invokeExceededTimes(String key, int num,int count){
        System.out.println("key值为"+key);
        boolean redisKey = redisTemplate.hasKey(key);
        if(redisKey){
            int times = Integer.parseInt(redisTemplate.opsForValue().get(key));
            if(times>=count){
                return false;
            }
            Long increment = redisTemplate.opsForValue().increment(key, 1);
            System.out.println("第"+increment+"次");
            return true;
        }else {
            redisTemplate.opsForValue().set(key,"1",num,TimeUnit.SECONDS);
        }
        return true;
    }
}
