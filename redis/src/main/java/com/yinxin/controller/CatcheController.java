package com.yinxin.controller;

import com.yinxin.service.CatcheService;
import com.yinxin.until.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2610:53
 */
@RestController
public class CatcheController {

    @Autowired
    CatcheService catcheService;


    /**
     * 缓存预热
     * @param id
     * @return
     */
    @GetMapping("/hotCache")
    public JsonData hotCache(Integer id){
        return catcheService.hotCache(id);
    }

    /**
     * 无缓存
     * @param id
     * @return
     */
    @GetMapping("/getInfoNoCache")
    @ResponseBody
    public JsonData getInfoNoCache(Integer id){
        return catcheService.getInfoNoCache(id);
    }

    /**
     * 有缓存
     * @param id
     * @return
     */
    @GetMapping("/getInfoWithCache")
    public JsonData getInfoWithCache(Integer id){
        return catcheService.getInfoWithCache(id);
    }


    /**
     * springCache作为缓存
     */
    @GetMapping("getCacheOrUser")
    public JsonData getCacheOrUser(Integer id){

        return JsonData.buildSuccess();
    }


}
