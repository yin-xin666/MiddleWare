package com.yinxin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxin.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2611:13
 */
@Mapper
public interface UserDao extends BaseMapper<User> {
    User getOne(Integer id);
}
