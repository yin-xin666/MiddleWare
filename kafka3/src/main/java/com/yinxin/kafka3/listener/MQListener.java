package com.yinxin.kafka3.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * @author yinxin
 * @version 1.0
 * @Description: mqlistener
 * @date 2022/3/1421:27
 */
@Component
public class MQListener {

    @KafkaListener(topics = {"xdclass-partion5"}, groupId = "xdclass-gp1")
    public void onMessage(ConsumerRecord<?, ?> record, Acknowledgment ack,
                          @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {
        // 打印出消息内容
        System.out.println("消费：topic"+record.topic()+"-partition:"+record.partition()+"-record:"+record.value());
        ack.acknowledge();
    }
}
