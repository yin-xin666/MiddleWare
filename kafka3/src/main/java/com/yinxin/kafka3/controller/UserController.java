package com.yinxin.kafka3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1420:39
 */
@RestController
public class UserController {
    private static final String TOPIC_NAME = "xdclass-partion5";

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    /**
     * 发送消息
     * @param phone
     */
    @GetMapping("/api/user/{phone}")
    public void sendMessage1(@PathVariable("phone") String phone) {
        kafkaTemplate.send(TOPIC_NAME, phone).addCallback(success -> {
            // 消息发送到的topic
            String topic = success.getRecordMetadata().topic();
            // 消息发送到的分区
            int partition = success.getRecordMetadata().partition();
            // 消息在分区内的offset
            long offset = success.getRecordMetadata().offset();
            System.out.println("发送消息成功:" + topic + "-" + partition + "-" + offset);
        }, failure -> {
            System.out.println("发送消息失败:" + failure.getMessage());
        });
    }

    /**
     * 注解方式的事务
     * @param num
     */
    @GetMapping("/api/user/tran1")
    @Transactional(rollbackFor = RuntimeException.class)
    public void sendMessage2(@RequestParam("num") int num) {
        kafkaTemplate.send(TOPIC_NAME,"这个是事务消息 1 i="+num);
        if(num == 0){
            throw new RuntimeException();
        }
        kafkaTemplate.send(TOPIC_NAME,"这个是事务消息 2 i="+num);
    }

    /**
     * kafka内部的事务发送消息
     * @param num
     */
    @GetMapping("/api/user/tran2}")
    public void sendMessage3(@RequestParam("num") int num) {
        kafkaTemplate.executeInTransaction(new KafkaOperations.OperationsCallback<String, Object, Object>() {
            @Override
            public Object doInOperations(KafkaOperations<String, Object> kafkaOperations) {
                kafkaOperations.send(TOPIC_NAME,"这是事务消息 1 i="+num);
                if(num == 0){
                    throw new RuntimeException();
                }
                kafkaTemplate.send(TOPIC_NAME,"这个是事务消息 2 i="+num);
                return true;
            }
        });
        System.out.println(1);
    }
}
