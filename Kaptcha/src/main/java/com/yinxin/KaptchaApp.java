package com.yinxin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/2516:30
 */
@SpringBootApplication
public class KaptchaApp {
    public static void main(String[] args) {
        SpringApplication.run(KaptchaApp.class,args);
    }
}
