package com.example.myktgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyktGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyktGatewayApplication.class, args);
    }

}
