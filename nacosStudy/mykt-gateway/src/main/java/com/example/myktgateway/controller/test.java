package com.example.myktgateway.controller;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;


/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/710:29
 */
@Controller
public class test {

    //限流规则名称
    public static final String GETORDER_KEY = "orderToMember";

    @RequestMapping("/test")
    @ResponseBody
    public String test1() {
        Entry entry = null;
        try {
            entry = SphU.entry(GETORDER_KEY);
            return "网关服务启动成功！";
        } catch (BlockException e) {
            //限流的情况就会进入到Exception
            return "当前网关访问人数过多，请稍后重试!";
        } finally {
            //SphU.entry(xxx)需要与entryexit()成对出现否则会导致调用链记录异常
            if (entry != null) {
                entry.exit();
            }
        }
    }


    //创建我们的限流规则（手动实现限流）
    @RequestMapping("/initFlowQpsRule")
    @ResponseBody
    public String initFlowQpsRule() {
        List<FlowRule> rules = new ArrayList<FlowRule>();
        FlowRule rule1 = new FlowRule();
        rule1.setResource(GETORDER_KEY);//QPS控制在2以内
        rule1.setCount(1);//QPS限流
        rule1.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule1.setLimitApp("default");
        rules.add(rule1);
        FlowRuleManager.loadRules(rules);
        return "....限流配置初始化成功..";
    }
}
