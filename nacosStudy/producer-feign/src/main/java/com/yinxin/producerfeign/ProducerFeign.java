package com.yinxin.producerfeign;


//import com.yinxin.producerfeign.config.FeignMultipartSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1814:40
 */
@FeignClient(name = "producer")
//,configuration = FeignMultipartSupportConfig.class网上找到这样的配置去解决方案但是不加这个也能成功
public interface ProducerFeign {
    @PostMapping(value = "/getFileInfo",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    List<Map<String, Object>> getFile(@RequestPart(required = true,name = "file") MultipartFile file,
                                      @RequestParam("startRow") Integer startRow,
                                      @RequestParam("endRow") Integer endRow);
    @GetMapping(value = "/getMsg")
    String getMsg(@RequestParam("msg") String msg);
}
