package com.yinxin.sentinelproducer.controller;


import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRuleManager;
import com.sun.org.apache.xalan.internal.xsltc.dom.CurrentNodeListIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1810:53
 */
@RestController
public class ProducerController {

    @Value("${server.port}")
    private String port;
    //限流规则名称
    public static final String THE_KEY = "key";

    static {
        ParamFlowRule rule = new ParamFlowRule(THE_KEY)
                .setParamIdx(0)
                .setGrade(RuleConstant.FLOW_GRADE_QPS)
                .setCount(1);
        ParamFlowRuleManager.loadRules(Collections.singletonList(rule));
        System.out.println("THE_KEY默认资源限流开启");
    }

    //注解方式实现限流
    @SentinelResource(value = THE_KEY, blockHandler = "getProducerPortQpsException")
    @RequestMapping("/getProducerPortSentinelResource")
    public String getProducerPortSentinelResource() {
        return "注解方式实现限流:getProducerPortSentinelResource";
    }

    public String getProducerPortQpsException(BlockException e) {
        e.printStackTrace();
        return "这个接口被限流了";
    }


    /**
     * 访问次数太多导致的熔断服务降级，sentinel dashboard配置相应策略
     * @param
     * @return
     */
    @SentinelResource(value = THE_KEY, blockHandler = "getProducerPortDowngradeRtType")
    @GetMapping(value = "/getProducerPortDowngradeTimesType")
    String getProducerPort3() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "正常执行我们业务逻辑！";
    }
    public String getProducerPortDowngradeRtType(BlockException e) {
        e.printStackTrace();
        return "访问次数太多,暂时无法访问该接口";
    }

    /**
     * 错误异常次数过多导致的熔断服务降级，sentinel dashboard配置相应策略
     * @param age
     * @return
     */
    @SentinelResource(value = "getProducerPortDowngradeErrorType", fallback="getProducerPortDowngradeErrorTypeFallback")
            @RequestMapping( "/getProducerPortDowngradeErrorType")
            public String get0rderDowngradeErrorType(int age) {
        int j = 1 / age;
        return "正常执行我们业务逻辑:j"+j;
    }
    public String getProducerPortDowngradeErrorTypeFallback(int age) {
        return "错误率太高,暂时无法访问该接口"+age;
    }
    /**
     * 测试gateway接口2
     * @param
     * @return
     */
    @GetMapping(value = "/getProducerPort2")
    String getProducerPort2() {
        return "访问到了producer中的信息" + port;
    }

    /**
     * 测试gateway接口1
     * @param
     * @return
     */
    @GetMapping(value = "/getProducerPort1")
    String getProducerPort1() {

        Entry entry = null;
        try {
            entry = SphU.entry(THE_KEY);
            return "访问到了producer中的信息" + port;
        } catch (BlockException e) {
            //限流的情况就会进入到Exception
            return "当前producer服务访问人数过多，请稍后重试!";
        } finally {
            //SphU.entry(xxx)需要与entryexit()成对出现否则会导致调用链记录异常
            if (entry != null) {
                entry.exit();
            }
        }
    }


}
