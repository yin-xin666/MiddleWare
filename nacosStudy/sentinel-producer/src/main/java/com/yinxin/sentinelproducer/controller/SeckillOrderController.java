package com.yinxin.sentinelproducer.controller;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.EntryType;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRuleManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 热词限流
 * @date 2022/4/814:31
 */
@RestController
public class SeckillOrderController {


    //秒杀限流规则
    public static final String SECKILL_KEY = "seckill_key";

    //秒杀接口限流配置策略
    static {
        ParamFlowRule rule = new ParamFlowRule(SECKILL_KEY)
                .setParamIdx(0)
                .setGrade(RuleConstant.FLOW_GRADE_QPS)
                .setCount(1);
        ParamFlowRuleManager.loadRules(Collections.singletonList(rule));
        System.out.println(">>>>秒杀接口限流策略配置成功<<<<");
    }
    @RequestMapping("/seckill")
    public String seckill(Long userId, Long orderId) {
        //把接口和热词规则的方法匹配上
        try {
            Entry entry = SphU.entry(SECKILL_KEY, EntryType.IN, 1, userId, orderId);
        } catch (BlockException e) {
            e.printStackTrace();
            return "当前用户访问频率太高，秒杀失败！";
        }
        return "秒杀成功！";
    }

    //秒杀限流规则
    public static final String SECKILL_RULE = "seckill_rule";
    @RequestMapping("/seckill2")
    @SentinelResource(value = SECKILL_RULE,fallback = "hotWordsFallBack",blockHandler = "hotWordsBlockHandler")
    public String seckill2(Long userId, Long orderId) {
        return "秒杀成功！"+userId+orderId;
    }

    private String hotWordsFallBack(){
        return "同一用户不可重复秒杀！";
    }
    private String hotWordsBlockHandler(){
        return "访问次数过多！";
    }
}
