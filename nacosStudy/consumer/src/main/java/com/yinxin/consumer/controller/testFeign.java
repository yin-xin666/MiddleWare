package com.yinxin.consumer.controller;


import com.yinxin.consumer.entity.Result;
import com.yinxin.producerfeign.ProducerFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1811:15
 */
@RestController
public class testFeign {
    @Autowired
    ProducerFeign producerFeign;
    @PostMapping("/testFeign")
    public Result testFeign(@RequestParam("file") MultipartFile file, @RequestParam("startRow")Integer startRow, @RequestParam("endRow")Integer endRow){
        List<Map<String, Object>> maps = producerFeign.getFile(file, startRow, endRow);
        return Result.success(maps);
    }

    @GetMapping("/testFeign2")
    public String testFeign2(@RequestParam("msg")String msg){
        String msg1 = producerFeign.getMsg(msg);
        return msg1;
    }
}
