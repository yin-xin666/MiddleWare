package com.yinxin.consumer.entity;

import java.io.Serializable;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1811:07
 */
public class Result implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer code;
    private String message;
    private Object Data;

    public static Result success(Object data){
        return new Result(200,"访问成功！",data);
    }

    public static Result err(Object data){
        return new Result(500,"访问失败！",data);
    }

    public Result() {
    }

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        Data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getDate() {
        return Data;
    }

    public void setDate(Object date) {
        Data = date;
    }
}
