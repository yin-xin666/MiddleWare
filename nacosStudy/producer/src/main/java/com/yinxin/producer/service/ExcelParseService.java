package com.yinxin.producer.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1811:01
 */
public interface ExcelParseService {
    List<Map<String, Object>> parse(MultipartFile file, Integer startRow, Integer endRow);
}
