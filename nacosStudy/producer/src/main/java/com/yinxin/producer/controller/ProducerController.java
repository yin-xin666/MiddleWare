package com.yinxin.producer.controller;


import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.yinxin.producer.service.ExcelParseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1810:53
 */
@RestController
public class ProducerController {

    @Value("${producer.msg}")
    private String msg;
    //限流规则名称
    public static final String GETORDER_KEY = "orderToMember";

    //创建我们的限流规则(手写)
   /* @RequestMapping("/initFlowQpsRule")
    public String initFlowQpsRule() {
        List<FlowRule> rules = new ArrayList<FlowRule>();
        FlowRule rule1 = new FlowRule();
        rule1.setResource(GETORDER_KEY);//QPS控制在2以内
        rule1.setCount(1);//QPS限流
        rule1.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule1.setLimitApp("default");
        rules.add(rule1);
        FlowRuleManager.loadRules(rules);
        return "....限流配置初始化成功..";
    }*/

    //注解方式实现限流
    @SentinelResource(value = GETORDER_KEY, blockHandler = "getProducerPortQpsException")
    @RequestMapping("getProducerPortSentinelResource")
    public String getProducerPortSentinelResource() {
        return "注解方式实现限流:getProducerPortSentinelResource";
    }

    public String getProducerPortQpsException(BlockException e) {
        e.printStackTrace();
        return "这个接口被限流了";
    }

    @Value("${server.port}")
    private String port;


    /**
     * 测试gateway接口2
     *
     * @param
     * @return
     */
    @GetMapping(value = "/getProducerPort2")
    String getProducerPort2() {
        return "访问到了producer中的信息" + port;
    }

    /**
     * 测试gateway接口1
     *
     * @param
     * @return
     */
    @GetMapping(value = "/getProducerPort1")
    String getProducerPort1() {
        Entry entry = null;
        try {
            entry = SphU.entry(GETORDER_KEY);
            return "访问到了producer中的信息" + port;
        } catch (BlockException e) {
            //限流的情况就会进入到Exception
            return "当前producer服务访问人数过多，请稍后重试!";
        } finally {
            //SphU.entry(xxx)需要与entryexit()成对出现否则会导致调用链记录异常
            if (entry != null) {
                entry.exit();
            }
        }
    }

    @Autowired
    ExcelParseService excelParseService;

    @PostMapping(path = "/getFileInfo")
    public List<Map<String, Object>> getFile(@RequestPart(required = true, name = "file") MultipartFile file,
                                             @RequestParam("startRow") Integer startRow,
                                             @RequestParam("endRow") Integer endRow) {
        List<Map<String, Object>> result = excelParseService.parse(file, startRow, endRow);
        return result;
    }


    /**
     * 配置中心上取数据的接口
     *
     * @param
     * @return
     */
    @GetMapping(value = "/getMsg")
    String getMsg() {
        return "访问到了producer中的信息:" + msg;
    }
}
