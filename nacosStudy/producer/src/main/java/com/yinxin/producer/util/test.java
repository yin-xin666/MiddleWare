package com.yinxin.producer.util;

import org.springframework.util.StringUtils;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/2220:28
 */
public class test {
    public static void main(String[] args) {
        String s = "  `yearmonth` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '年份月份如2022-03',\n" +
                "  `project_id` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT '项目id',\n" +
                "  `period_id` varchar(2) DEFAULT NULL COMMENT '期次',\n" +
                "  `office_id` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '所属机构id',\n" +
                "  `office_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '所属部门名',\n" +
                "  `project_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目名',\n" +
                "  `product_group` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '产品线',\n" +
                "  `product_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '产品名',\n" +
                "  `first_type` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '大类',\n" +
                "  `second_type` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '小类',\n" +
                "  `pm` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目经理名字',\n" +
                "  `pe` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目主管名字',\n" +
                "  `product_man` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '产品经理名',\n" +
                "  `qa` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT 'QA名',\n" +
                "  `contract_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '合同名称',\n" +
                "  `english_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '合同英文名',\n" +
                "  `contract_code` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '财务编码',\n" +
                "  `contract_amount` decimal(12,2) DEFAULT NULL COMMENT '合同额',\n" +
                "  `tax` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '税金',\n" +
                "  `svn_url` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT 'svn',\n" +
                "  `project_stage_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目状态',\n" +
                "  `year_begin_progress` decimal(4,2) DEFAULT NULL COMMENT '年初进度',\n" +
                "  `season_begin_progress` decimal(4,2) DEFAULT NULL COMMENT '季初进度',\n" +
                "  `last_month_progress` decimal(4,2) DEFAULT NULL COMMENT '上月进度',\n" +
                "  `next_month_progress` decimal(4,2) DEFAULT NULL COMMENT '预计下月进度',\n" +
                "  `season_end_progress` decimal(4,2) DEFAULT NULL COMMENT '预计季末进度',\n" +
                "  `year_end_progress` decimal(4,2) DEFAULT NULL COMMENT '预计年末进度',\n" +
                "  `this_month_money` decimal(12,2) DEFAULT NULL COMMENT '本月产值',\n" +
                "  `this_year_finish_money` decimal(12,2) DEFAULT NULL COMMENT '已完成年度产值',\n" +
                "  `this_season_finish_money` decimal(12,2) DEFAULT NULL COMMENT '已完成季度度产值',\n" +
                "  `next_month_money` decimal(12,2) DEFAULT NULL COMMENT '预计下月年度产值',\n" +
                "  `season_end_money` decimal(12,2) DEFAULT NULL COMMENT '预计季末年度产值',\n" +
                "  `year_end_money` decimal(12,2) DEFAULT NULL COMMENT '预计年末年度产值',\n" +
                "  `avg_month_user_count` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '本年平均月度人数',\n" +
                "  `avg_month_money` decimal(12,2) DEFAULT NULL COMMENT '每人平均月产值',\n" +
                "  `plan_all_cost` decimal(12,2) DEFAULT NULL COMMENT '计划总成本（人月）',\n" +
                "  `real_all_cost` decimal(12,2) DEFAULT NULL COMMENT '实际成本（人月）来源于网上日报的人月数乘以人员系数，1人月=20.67人日，1人日=8小时，工作日加班不算，周末加班以日报为准',\n" +
                "  `this_month_cost` decimal(12,2) DEFAULT NULL COMMENT '本月成本（人月）',\n" +
                "  `plan_progress` decimal(4,2) DEFAULT NULL COMMENT '计划进度',\n" +
                "  `real_progress` decimal(4,2) DEFAULT NULL COMMENT '实际进度',\n" +
                "  `plan_cost` decimal(12,2) DEFAULT NULL COMMENT '计划成本（人月）',\n" +
                "  `sub_progress` decimal(4,2) DEFAULT NULL COMMENT '进度偏差（计划进度-实际进度）',\n" +
                "  `cost_cpi` decimal(12,2) DEFAULT NULL COMMENT '质量成本绩效指标CPI=计划成本/实际成本',\n" +
                "  `establish_date` date DEFAULT NULL COMMENT '立项日期',\n" +
                "  `plan_deploy_date` date DEFAULT NULL COMMENT '合同上线日期',\n" +
                "  `plan_first_accept_date` date DEFAULT NULL COMMENT '合同初验日期',\n" +
                "  `plan_end_accept_date` date DEFAULT NULL COMMENT '合同终验日期',\n" +
                "  `real_deploy_date` date DEFAULT NULL COMMENT '系统上线日期',\n" +
                "  `real_first_accept_date` date DEFAULT NULL COMMENT '系统初验日期',\n" +
                "  `real_end_accept_date` date DEFAULT NULL COMMENT '系统终验日期',\n" +
                "  `progress_status` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目进展阶段',\n" +
                "  `qulity_deduct` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目进展阶段（0%<=当前进度<75% ：启动 75<=当前进度<95% ：上线 95%<=当前进度<100% ：初验 100%=当前进度 : 终验）',\n" +
                "  `satisfaction` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '质量问题扣分',\n" +
                "  `quality_addscore` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '质量绩效加分',\n" +
                "  `complain` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '重大投诉',\n" +
                "  `praise_knowledge` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '表扬和财富库贡献',\n" +
                "  `project_performance` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目绩效',\n" +
                "  `pro_user_count` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '项目人数',\n" +
                "  `pro_user_list` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '人员组成',\n" +
                "  `total_money` decimal(12,2) DEFAULT NULL COMMENT '累计产值(万元)',\n" +
                "  `total_cost` decimal(12,2) DEFAULT NULL COMMENT '累计成本（万元）',\n" +
                "  `m_rate` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '毛利率',\n" +
                "  `research_worktime` decimal(12,2) DEFAULT NULL COMMENT '研发工作量(人月)',\n" +
                "  `thisyear_real_costmoney` decimal(12,2) DEFAULT NULL COMMENT '当年实际成本(万元)',\n" +
                "  `pm_edit_time` datetime DEFAULT NULL COMMENT '项目经理编辑时间',\n" +
                "  `qa_edit_time` datetime DEFAULT NULL COMMENT 'qa编辑时间',\n" +
                "  `create_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建者',\n" +
                "  `create_time` datetime DEFAULT NULL COMMENT '创建时间',\n" +
                "  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '更新者',\n" +
                "  `update_time` datetime DEFAULT NULL COMMENT '更新时间',\n" +
                "  `status` int(1) DEFAULT '0' COMMENT '0未开始、1进行中、2已完成、3已暂停、4已取消、5已关闭',\n" +
                "  `remark` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',\n" +
                "  `del_flag` int(11) DEFAULT NULL COMMENT '删除标识',\n" +
                "  `del_token` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '删除 token',";
        String[] split = s.split("\n");
        for (String line : split) {
            if (StringUtils.hasText(line)) {
                line = line.trim();
                String[] split1 = line.split("\\s");
                String result = "private ";
                String fieldName = "";
                String type = "";
                for (String cl : split1) {
                    if (StringUtils.hasText(cl)) {
                        if (cl.startsWith("`")) {
                            fieldName = cl.replace("`", "");
                        } else if (cl.startsWith("int(")) {
                            type = "Integer";
                        } else if (cl.startsWith("varchar(")) {
                            type = "String";
                        } else if (cl.startsWith("decimal(")) {
                            type = "BigDecimal";
                        } else if (cl.equals("datetime")) {
                            type = "Date";
                        } else if (cl.equals("date")) {
                            type = "Date";
                        }
                    }
                }
                fieldName = getTuoFengName(fieldName);
                String comment = line.split(" COMMENT ")[1].trim();
                comment = comment.substring(0, comment.length() - 1).replace("'", "\"");
                result = result + " " + type + " " + fieldName + " ;";
                System.out.println("@ApiModelProperty(value = " + comment + ")");
                System.out.println(result);
            }
        }
    }

    private static String getTuoFengName(String name) {
        String[] s = name.split("_");
        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (String c : s) {
            if (StringUtils.hasText(c) && isFirst) {
                isFirst = false;
                sb.append(c);
            } else if (StringUtils.hasText(c) && !isFirst) {
                if (c.length() > 1) {
                    sb.append(c.substring(0, 1).toUpperCase() + c.substring(1));
                } else {
                    sb.append(c.substring(0, 1).toUpperCase());
                }
            }
        }
        return sb.toString();
    }

}
