package com.yinxin.producer.service.impl;

import com.yinxin.producer.service.ExcelParseService;
import com.yinxin.producer.util.ExcelUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1811:00
 */
@Service
public class ExcelParseServiceImpl implements ExcelParseService {
    @Override
    public List<Map<String, Object>> parse(MultipartFile file,Integer startRow,Integer endRow) {
        String originalFilename = file.getOriginalFilename();
        String contentType = file.getContentType();
        String fileName = file.getName();
        InputStream inputStream;

        List<Map<String, Object>> maps=new ArrayList<>();
        try {
            inputStream = file.getInputStream();
            if(originalFilename.endsWith(".xls")||originalFilename.endsWith(".xlsx")){
                maps = ExcelUtil.readeExcelData(inputStream, 0, 0, startRow, endRow);
            }else {
                return maps;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return maps;
        }
        HashMap<String, Object> excelInfo = new HashMap<>();
        excelInfo.put("文件名",originalFilename);
        excelInfo.put("文件类型",contentType);
        maps.add(excelInfo);
        return maps;
    }
}
