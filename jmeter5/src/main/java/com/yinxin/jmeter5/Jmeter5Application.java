package com.yinxin.jmeter5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jmeter5Application {

    public static void main(String[] args) {
        SpringApplication.run(Jmeter5Application.class, args);
    }

}
