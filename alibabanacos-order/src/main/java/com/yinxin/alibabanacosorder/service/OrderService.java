package com.yinxin.alibabanacosorder.service;

import com.yinxin.alibabanacosorder.config.LoadBalancer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 服务的调用者
 * @date 2022/3/179:46
 */
@RestController
public class OrderService {


    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    LoadBalancer loadBalancer;

    @Autowired
    LoadBalancerClient loadBalancerClient;
    /**
     * order调用member服务接口
     * @return
     */
    @GetMapping("/orderToMenber")
    public String orderToMember(){
        //1、根据服务名称从注册中心获取集群列表地址
        List<ServiceInstance> instances = discoveryClient.getInstances("member");
        // 用自己的负载均衡器去从集群选取一个地址
        ServiceInstance serviceInstance = loadBalancer.getSingleAddress(instances);
        //获取服务url和端口号
        URI rpcMemberUrl = serviceInstance.getUri();
        String result = restTemplate.getForObject(rpcMemberUrl+"/getUser", String.class);
        //2、列表任意选择一个 实现本地rpc调用rest
        return result+serviceInstance.getPort();
    }

    /**
     *加@LoadBalanced注解实现负载均衡
     * @return
     */
    @GetMapping("/orderToRibbonMenber")
    public String orderToRibbonMember(){
        String result = restTemplate.getForObject("http://member/getUser", String.class);
        //2、列表任意选择一个 实现本地rpc调用rest
        return result;
    }

    /**
     * loadBalancerClient实现负载均衡
     * @return
     */
    @GetMapping("/loadBalancerMember")
    public String loadBalancerMember(){
        ServiceInstance serviceInstance = loadBalancerClient.choose("member");
        //1、获取服务url和端口号
        URI rpcMemberUrl = serviceInstance.getUri();
        String result = restTemplate.getForObject(rpcMemberUrl+"/getUser", String.class);
        //2、列表任意选择一个 实现本地rpc调用rest
        return result+serviceInstance.getPort();
    }
}
