package com.yinxin.alibabanacosorder.config;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 自定义手写的负载均衡器
 * @date 2022/3/1710:57
 */

public interface LoadBalancer {
    /**
     * 根据不同的地址，返回单个调用rpc地址
     * @param serviceInstances
     * @return
     */
    ServiceInstance getSingleAddress(List<ServiceInstance> serviceInstances);
}
