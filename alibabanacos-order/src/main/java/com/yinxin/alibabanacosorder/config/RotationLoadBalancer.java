package com.yinxin.alibabanacosorder.config;


import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1711:00
 */
@Component
public class RotationLoadBalancer implements LoadBalancer{

    //从0开始去计数
    private AtomicInteger atomicInteger=new AtomicInteger(0);

    @Override
    public ServiceInstance getSingleAddress(List<ServiceInstance> serviceInstances) {
        int index = atomicInteger.incrementAndGet() % serviceInstances.size();
        return serviceInstances.get(index);
    }
}
