package com.yinxin.alibabanacosorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class AlibabanacosOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlibabanacosOrderApplication.class, args);
    }
    /**
     * @Description: 手动注入RestTemplate
     *
     * <p>启动报错：找不到RestTemplate
     *
     * @auther: zpq
     * @date: 2020/11/5 4:13 下午
     */
    //加LoadBalanced实现负载均衡
    @Bean
//    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
