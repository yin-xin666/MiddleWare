package service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/3/1611:40
 */
@WebService
public class UserService {

    @WebMethod
    public String getUser(long id){
        return "id是:"+id;
    }

    //这个是jdk自带的服务注册的注解
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9200/service/UserService",new UserService());
        System.out.println("服务发布成功！");
    }

}
