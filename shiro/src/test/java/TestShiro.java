import com.yinxin.realm.Realm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2023/8/2514:07
 */
public class TestShiro {
    @Test
    public void testLogin1(){
        //创建Shiro的安全管理器，是shiro的核心
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
        //加载shiro.ini配置，得到配置中的用户信息（账号+密码）
        IniRealm iniRealm = new IniRealm("classpath:shiro.ini");
        securityManager.setRealm(iniRealm);
        //把安全管理器注入到当前的环境中
        SecurityUtils.setSecurityManager(securityManager);
        //无论有无登录都可以获取到subject主体对象，但是判断登录状态需要利用里面的属性来判断
        Subject subject = SecurityUtils.getSubject();
        System.out.println("没登录时认证状态："+subject.isAuthenticated());
        //创建令牌(携带登录用户的账号和密码)
        UsernamePasswordToken token = new UsernamePasswordToken("yinxin","123456");
        //执行登录操作(将用户的和 ini 配置中的账号密码做匹配)
        subject.login(token);
        System.out.println("登录后认证状态："+subject.isAuthenticated());
        //登出
        subject.logout();
        System.out.println("登出认证状态："+subject.isAuthenticated());
    }
    @Test
    public void testLogin2(){
        //创建Shiro的安全管理器，是shiro的核心
        DefaultSecurityManager securityManager = new DefaultSecurityManager();
        //自定义Realm,查出用户信息
        Realm realm = new Realm();
        securityManager.setRealm(realm);
        //把安全管理器注入到当前的环境中
        SecurityUtils.setSecurityManager(securityManager);
        //无论有无登录都可以获取到subject主体对象，但是判断登录状态需要利用里面的属性来判断
        Subject subject = SecurityUtils.getSubject();
        System.out.println("没登录时认证状态："+subject.isAuthenticated());
        //创建令牌(携带登录用户的账号和密码)
        UsernamePasswordToken token = new UsernamePasswordToken("yinxin","123456");
        //执行登录操作(将用户的和 ini 配置中的账号密码做匹配)
        subject.login(token);
        System.out.println("登录后认证状态："+subject.isAuthenticated());
        //登出
        subject.logout();
        System.out.println("登出认证状态："+subject.isAuthenticated());

//        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        
    }

}
