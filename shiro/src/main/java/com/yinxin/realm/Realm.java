package com.yinxin.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2023/8/2514:32
 */
public class Realm extends AuthorizingRealm {

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //1.获取登录用户名
        String username = (String) token.getPrincipal();
        //2.以用户名为条件查询mysql数据库,得到用户对象
        //模拟数据
        Employee employee = new Employee();
        employee.setUsername("yinxin");
        employee.setPassword("123456");
        //封装成一个认证info对象
        //判断用户是否为空
        if (employee!=null){
            return new SimpleAuthenticationInfo(
                    employee,
                    employee.getPassword(),
                    super.getName()
            );
        }
        return null;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }
}
