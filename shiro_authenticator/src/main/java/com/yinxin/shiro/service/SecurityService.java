package com.yinxin.shiro.service;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/1915:26
 */
public interface SecurityService {

    String findPasswordByLoginName(String loginName);
}
