package com.yinxin.shiro.tools;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.codec.Hex;

import java.util.Arrays;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/1917:17
 */
public class EncodesUtil {

    public static String encodeHex(byte[] input){
        return Hex.encodeToString(input);
    }

    public static byte[] decodeHex(String input){
        return Hex.decode(input);
    }

    public static String encodeBase64(byte[] input){
        return Base64.encodeToString(input);
    }


    public static void main(String[] args) {
        String input = "yinxin";
        String flag = encodeHex(input.getBytes());
        System.out.println(flag);
        String valhander = new String(decodeHex(flag));
        System.out.println("valhander = " + valhander);
    }

}
