import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.junit.Test;

/**
 * @author yinxin
 * @version 1.0
 * @Description: shiro的第一个例子
 * @date 2022/5/1914:43
 */
public class HelloShiro {


    @Test
    public void shiroLogin(){
        //1、导入INI配置创建工厂
        Factory<SecurityManager> factory = new IniSecurityManagerFactory("classpath:shiro.ini");
        //2、工厂构建安全管理器
        SecurityManager securityManager = factory.getInstance();
        //3、使用工具生效安全管理器
        SecurityUtils.setSecurityManager(securityManager);
        //4、使用工具获得subject主体
        Subject subject = SecurityUtils.getSubject();
        //5、构建账户密码
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken("yinxin","123456");
        System.out.println(usernamePasswordToken);
        //6、使用subject主体登录
        subject.login(usernamePasswordToken);
        //7、打印登录信息
        System.out.println("登录结果"+subject.isAuthenticated());
    }
}
