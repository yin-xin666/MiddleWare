package com.yinxin;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2023/9/615:36
 */
public class CsvDemo {
   public static void main(String[] args) throws IOException {
      //1、加载a.csv文件
      ClassLoader classLoader = CsvDemo.class.getClassLoader();
      URL resource = classLoader.getResource("a.csv");
      FileReader fileReader = new FileReader(resource.getFile());
      //2、解析csv文件
      Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(fileReader);
      for (CSVRecord record : records) {
         System.out.println("第一行:"+
                 record.get(0)+","+
                 record.get(1)+","+
                 record.get(2)+","+
                 record.get(3)+","+
                 record.get(4)+","+
                 record.get(5)+":"
                 +"结束。");
      }

   }
}
