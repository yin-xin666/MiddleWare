package com.yinxin;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.extern.slf4j.Slf4j;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/10/1315:35
 */
public class ExcleDemoListener extends AnalysisEventListener<DataDemo> {
    @Override
    public void invoke(DataDemo dataDemo, AnalysisContext analysisContext) {
        System.out.println("解析到一条数据:"+ dataDemo.toString());
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println("所有数据解析完成！");
    }
}
