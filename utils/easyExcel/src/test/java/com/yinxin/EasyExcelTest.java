package com.yinxin;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/10/1314:53
 */
public class EasyExcelTest {


    private String getResourcePath(){
//        return this.getClass().getClassLoader().getResource("").getPath();
        return "D:\\Code\\MyStudy\\MiddleWare\\utils\\easyExcel\\src\\main\\resources\\";
    }
    /**
     * 循环制造list集合数据
     *
     * @return
     */
    private List<DataDemo> data() {
        List<DataDemo> list = new ArrayList<>();

        //算上标题，做多可写65536行
        //超出：java.lang.IllegalArgumentException: Invalid row number (65536) outside allowable range (0..65535)
        for (int i = 0; i < 5; i++) {
            DataDemo data = new DataDemo();
            data.setName("Helen" + i);
            data.setBirthday(new Date());
            data.setSalary(123456.1234);
            list.add(data);
        }
        return list;
    }

    /**
     * 写入xlsx文件测试方法
     */
    @Test
    public void simpleWriteXlsx() {
        String fileName = getResourcePath()+"demoExcel2.xlsx"; //需要提前新建目录
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        EasyExcel.write(fileName, DataDemo.class).sheet("模板").doWrite(data());
    }

    /**
     * 写入xls文件测试方法
     */
    @Test
    public void simpleWriteXls() {
        String fileName = getResourcePath()+"demoExcel1.xls";
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, DataDemo.class).excelType(ExcelTypeEnum.XLS).sheet("模板").doWrite(data());
    }

    /**
     * 读取xlsx文件最简单的读
     */
    @Test
    public void simpleReadXlsx() {
        String fileName = getResourcePath()+"demoExcel2.xlsx";
        // 这里默认读取第一个sheet
        EasyExcel.read(fileName, DataDemo.class, new ExcleDemoListener()).sheet().doRead();
    }

    /**
     * 读取xls文件最简单的读
     */
    @Test
    public void simpleReadXls() {
        String fileName = getResourcePath()+"demoExcel1.xls";
        EasyExcel.read(fileName, DataDemo.class, new ExcleDemoListener()).excelType(ExcelTypeEnum.XLS).sheet().doRead();
    }
}
