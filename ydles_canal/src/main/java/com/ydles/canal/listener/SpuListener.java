package com.ydles.canal.listener;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.xpand.starter.canal.annotation.CanalEventListener;
import com.xpand.starter.canal.annotation.ListenPoint;
import com.ydles.canal.config.RabbitMQConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * @Created by IT李老师
 * 公主号 “IT李哥交朋友”
 * 个人微 itlils
 */
@CanalEventListener
public class SpuListener {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * spu 表更新
     * @param eventType
     * @param rowData
     */
    @ListenPoint(schema = "ydles_goods", table = {"tb_spu"},eventType = CanalEntry.EventType.UPDATE )
    public void spuUp(CanalEntry.EventType eventType, CanalEntry.RowData rowData) {
        System.err.println("tb_spu表数据发生变化");

        //修改前数据
        Map<String,String> oldMap=new HashMap<>();
//        for(CanalEntry.Column column: rowData.getBeforeColumnsList()) {
//            oldMap.put(column.getName(),column.getValue());
//        }
        rowData.getBeforeColumnsList().forEach(column -> oldMap.put(column.getName(),column.getValue()));

        //修改后数据
        Map<String,String> newMap=new HashMap<>();
//        for(CanalEntry.Column column: rowData.getAfterColumnsList()) {
//            newMap.put(column.getName(),column.getValue());
//        }
        rowData.getAfterColumnsList().forEach(column -> newMap.put(column.getName(),column.getValue()));

        //is_marketable  由0改为1表示上架
        if("0".equals(oldMap.get("is_marketable")) && "1".equals(newMap.get("is_marketable"))){
            String spuId = newMap.get("id");
            System.out.println("商品上架了,向mq发消息"+spuId);
            //发送到mq商品上架交换器上
            rabbitTemplate.convertAndSend(RabbitMQConfig.GOODS_UP_EXCHANGE,"",spuId);
        }

        //is_marketable  由1改为0表示下架
        if("1".equals(oldMap.get("is_marketable")) && "0".equals(newMap.get("is_marketable"))){
            String spuId = newMap.get("id");
            System.out.println("商品下架了,向mq发消息"+spuId);
            //发送到mq商品上架交换器上
            rabbitTemplate.convertAndSend(RabbitMQConfig.GOODS_DOWN_EXCHANGE,"",spuId);
        }
    }
}
