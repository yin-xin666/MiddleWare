package org.example.client;

import org.example.tools.DigestsUtil;
import org.example.tools.EncodesUtil;
import org.junit.Test;

import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/5/3010:30
 */
public class ClientTest {

    @Test
    public void testHex(){
        String val = "12345";
        String flag = EncodesUtil.encodeHex(val.getBytes());
        String decode = new String(EncodesUtil.decodeHex(flag));
        System.out.println(decode.equals(val));
    }


    /**
     * hash散列算法
     */
    @Test
    public void testDigests(){
        Map<String, String> stringStringMap = DigestsUtil.entryptPassword("123456");
        System.out.println(stringStringMap);
    }
    
}
