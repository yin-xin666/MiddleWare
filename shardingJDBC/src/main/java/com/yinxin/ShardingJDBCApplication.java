package com.yinxin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 启动类
 * @date 2022/8/210:19
 */
@SpringBootApplication
@MapperScan("com.yinxin.mapper")
public class ShardingJDBCApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShardingJDBCApplication.class,args);
    }
}
