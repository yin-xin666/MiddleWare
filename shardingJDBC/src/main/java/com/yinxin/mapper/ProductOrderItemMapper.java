package com.yinxin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxin.model.ProductOrderItemDo;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/8/316:17
 */
public interface ProductOrderItemMapper extends BaseMapper<ProductOrderItemDo> {

}
