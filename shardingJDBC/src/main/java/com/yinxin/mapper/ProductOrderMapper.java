package com.yinxin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxin.model.ProductOrderDO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 商品描述类
 * @date 2022/8/210:25
 */
public interface ProductOrderMapper extends BaseMapper<ProductOrderDO> {

    @Select("select * from product_order o left join product_order_item i on o.id=i.product_order_id")
    List<Object> listProductOrderDetail();
}
