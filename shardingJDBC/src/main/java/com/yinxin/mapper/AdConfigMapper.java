package com.yinxin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxin.model.AdConfigDO;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/8/39:24
 */
public interface AdConfigMapper extends BaseMapper<AdConfigDO> {

}
