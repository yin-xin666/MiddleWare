package com.yinxin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yinxin.mapper.ProductOrderMapper;
import com.yinxin.model.ProductOrderDO;
import com.yinxin.utils.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/8/210:44
 */
@RestController
public class ShopOrderController {

    @Autowired
    ProductOrderMapper productOrderMapper;

    @GetMapping("/getOrder/{userId}")
    public JsonData getOrder(@PathVariable("userId")Long userId){
        Page<ProductOrderDO> page = new Page<>(1,5);
        /*Page<ProductOrderDO> productOrderDOPage =
                productOrderMapper.selectPage(page, new LambdaQueryWrapper<ProductOrderDO>().eq(ProductOrderDO::getUserId, userId));*/
        Page<ProductOrderDO> productOrderDOPage =
                productOrderMapper.selectPage(page,new LambdaQueryWrapper<ProductOrderDO>().eq(ProductOrderDO::getState,"Pay"));
        List<ProductOrderDO> records = productOrderDOPage.getRecords();
        return JsonData.buildSuccess(records);
    }
}
