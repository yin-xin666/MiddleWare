package com.yinxin.strategy;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 分库策略
 * @date 2022/8/413:45
 */
public class CustomDBPreciseShardingAlgorithm implements PreciseShardingAlgorithm<Long> {
    /**
     * @param dataSourceNames 数据源集合
     *                   在分库时值为所有分库表的集合 databaseNames
     *                   分表时为对应分片库中所有分片表的集合 tablesNames
     * @param preciseShardingValue 分片属性，包括
     *                                      logicTableName 为逻辑表，
     *                                      columnName分片键（字段），
     *                                      value为从SQL中解析出的分片键的值
     * @return
     */
    @Override
    public String doSharding(Collection<String> dataSourceNames, PreciseShardingValue<Long> preciseShardingValue) {
        for (String dataSourceName : dataSourceNames) {
            String value = preciseShardingValue.getValue() % dataSourceNames.size() + "";
            if(dataSourceName.endsWith(value)){
                return dataSourceName;
            }
        }
        throw new IllegalArgumentException();
    }
}
