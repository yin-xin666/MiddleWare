package com.yinxin.strategy;

import com.google.common.collect.Range;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingValue;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 范围查询策略
 * @date 2022/8/59:45
 */
public class CustomRangeShardingAlgorithm implements RangeShardingAlgorithm<Long> {
    /**
     * @param dataSourceNames 数据源集合
     *                   在分库时值为所有分库表的集合 databaseNames
     *                   分表时为对应分片库中所有分片表的集合 tablesNames
     * @param rangeShardingValue 范围分片属性，包括
     *                                      logicTableName 为逻辑表，
     *                                      columnName分片键（字段），
     *                                      value为从SQL中解析出的分片键的值
     * @return
     */
    @Override
    public Collection<String> doSharding(Collection<String> dataSourceNames, RangeShardingValue<Long> rangeShardingValue) {
        Set<String> result = new LinkedHashSet<>();
        Range<Long> valueRange = rangeShardingValue.getValueRange();
        //between 起始值
        Long lowerEndpoint = valueRange.lowerEndpoint();
        //between 结束值
        Long upperEndpoint = valueRange.upperEndpoint();

        for (Long i = lowerEndpoint; i <= upperEndpoint; i++) {
            for (String dataSource : dataSourceNames) {
                String value = i % dataSourceNames.size() + "";
                if(dataSource.endsWith(value)){
                    result.add(dataSource);
                }
            }
        }
        return result;
    }
}
