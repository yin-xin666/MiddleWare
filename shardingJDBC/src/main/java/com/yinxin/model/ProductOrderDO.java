package com.yinxin.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/8/210:18
 */
@Data
@TableName("product_order")
@EqualsAndHashCode(callSuper = false)
public class ProductOrderDO {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String outTradeNo;
    private String state;
    private Date createTime;
    private Double payAmount;
    private String nickname;
    private Long userId;
}
