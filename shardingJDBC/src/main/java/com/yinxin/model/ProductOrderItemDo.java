package com.yinxin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/8/316:06
 */
@Data
@TableName("product_order_item")
@EqualsAndHashCode(callSuper = false)
public class ProductOrderItemDo {
    private Long id;
    private Long productOrderId;
    private Long productId;
    private String productName;
    private Integer buyNum;
    private Long userId;
}
