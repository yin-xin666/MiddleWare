package com.example;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yinxin.ShardingJDBCApplication;
import com.yinxin.mapper.AdConfigMapper;
import com.yinxin.mapper.ProductOrderMapper;
import com.yinxin.model.AdConfigDO;
import com.yinxin.model.ProductOrderDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 测试类
 * @date 2022/8/110:05
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = ShardingJDBCApplication.class)
@Slf4j
public class DBTest {
    @Autowired
    ProductOrderMapper productOrderMapper;
    @Test
    public void testSaveProductOrder() {
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            ProductOrderDO productOrderDO = new ProductOrderDO();
            productOrderDO.setCreateTime(new Date());
            productOrderDO.setNickname("自定义水平分库分表 yinxin-" + i);
            productOrderDO.setOutTradeNo(UUID.randomUUID().toString().substring(0, 32));
            productOrderDO.setPayAmount(100.00);
            productOrderDO.setState("Pay");
            int value = random.nextInt(100);
            //分库用user_id
            productOrderDO.setUserId(Long.valueOf(value));

            productOrderMapper.insert(productOrderDO);
        }
    }

    @Autowired
    AdConfigMapper adConfigMapper;

    @Test
    public void testSaveAdConfig() {
//      for (int i = 0; i < 10; i++) {
        AdConfigDO adConfigDO = new AdConfigDO();
        adConfigDO.setConfigKey("banner");
        adConfigDO.setConfigValue("xdclass.net");
        adConfigDO.setType("hash");
        //id配置好了雪花算法自动生成;
        adConfigMapper.insert(adConfigDO);
//      }
    }

    /**
     * 绑定表(连表) 查询
     */
    @Test
    public void testBinding(){
        List<Object> objects = productOrderMapper.listProductOrderDetail();
        System.out.println(objects);
    }
    /**
     * 有分片键 查询
     */
    @Test
    public void testPartitionSelect(){
        //同一个表不同数据源
        //ds1 ::: SELECT  id,out_trade_no,state,create_time,pay_amount,nickname,user_id  FROM product_order_1 WHERE (id IN (?,?)) ::: [761630760191918081, 761630760242249729]
        //ds0 ::: SELECT  id,out_trade_no,state,create_time,pay_amount,nickname,user_id  FROM product_order_1 WHERE (id IN (?,?)) ::: [761630760191918081, 761630760242249729]
        List<ProductOrderDO> ProductOrderDOs = productOrderMapper.selectList(
                new QueryWrapper<ProductOrderDO>().in("id", Arrays.asList(761630760347107328L, 761630760389050368L)));
        System.out.println(ProductOrderDOs);
    }
    /**
     * 无分片键 查询
     */
    @Test
    public void testNoPartitionSelect(){
        //全表路由
        productOrderMapper.selectList(
                new QueryWrapper<ProductOrderDO>().eq("out_trade_no", "dcab0844-b9dc-4fc2-8bb7-41e31f1c"));
    }
    /**
     * 有分片键 删除
     */
    @Test
    public void testPartitionDel(){
        //同一个表不同数据源
        productOrderMapper.delete(
                new QueryWrapper<ProductOrderDO>().eq("id", 761630760217083904L));
    }

    /**
     * 有分片键 删除
     */
    @Test
    public void testNoPartitionDel(){
        //同一个表不同数据源
        productOrderMapper.delete(
                new QueryWrapper<ProductOrderDO>().eq("out_trade_no", "dcab0844-b9dc-4fc2-8bb7-41e31f1c"));
    }

    /**
     * 标准分片策略 范围分片
     */
    @Test
    public void testBetween(){
        //直接使用范围查询（SELECT  id,out_trade_no,state,create_time,pay_amount,nickname,user_id  FROM product_order     WHERE (id BETWEEN ? AND ?)）
        // 会报错：Cannot find range sharding strategy in sharding rule.
        List<ProductOrderDO> productOrderDOS = productOrderMapper.selectList(new QueryWrapper<ProductOrderDO>().between("id", 761959997541187584L, 761959997977395200L));
        System.out.println(productOrderDOS+"并且这个数组的大小是："+productOrderDOS.size());
    }
    /**
     * 复合分片键
     */
    @Test
    public void testMultiPartitionKeySelect(){
        productOrderMapper.selectList(
                new QueryWrapper<ProductOrderDO>().eq("id",761959997792845824L).eq("user_id",89L));
    }

    /**
     * 通过Hint代码指定的方式而非SQL解析的方式分片的策略(正常代码可以用AOP实现)
     * 适用于比较复杂的需要分片的查询，Hint分片策略性能可能会更好。
     */
    @Test
    public void testHit(){
        //清楚历史规则
        HintManager.clear();
        //获取对应实例
        HintManager hintManager = HintManager.getInstance();
        //设置库的分片键值 value用于库分片取模
        hintManager.addDatabaseShardingValue("product_order",3L);
        //设置表的分片键值 value是用于表的分片取模
        hintManager.addTableShardingValue("product_order",8L);
        //强制读主库
        hintManager.setMasterRouteOnly();
        //对应的库表路由取决于hintManager添加的value而不是id的值
        List<ProductOrderDO> productOrderDOS = productOrderMapper.selectList(new QueryWrapper<ProductOrderDO>().eq("id", 66L));
        System.err.println(productOrderDOS);

    }


    @Test
    public void test(){
        for (int i = 0; i < 100; i++) {
            System.out.println("i="+i);
        }
    }
}
