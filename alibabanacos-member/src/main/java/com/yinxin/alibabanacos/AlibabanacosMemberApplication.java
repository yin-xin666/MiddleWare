package com.yinxin.alibabanacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlibabanacosMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlibabanacosMemberApplication.class, args);
    }

}
