package com.yinxin.alibabanacos.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yinxin
 * @version 1.0
 * @Description: 被消费调用的服务
 * @date 2022/3/179:28
 */
@RestController
public class MemberService {

    /**
     * 被消费调用的服务的接口
     * @param userId
     * @return
     */
    @GetMapping("/getUser")
    public String getUser(Integer userId){

        return "MemberService的getUser方法";
    }
}
