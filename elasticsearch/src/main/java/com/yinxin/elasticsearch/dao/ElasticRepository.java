package com.yinxin.elasticsearch.dao;

import com.yinxin.elasticsearch.entity.SkuInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface ElasticRepository extends ElasticsearchRepository<SkuInfo,Long> {
}
