package com.yinxin.elasticsearch.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxin.elasticsearch.entity.Spu;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface SpuDao extends BaseMapper<Spu> {
}
