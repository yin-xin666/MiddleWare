package com.yinxin.elasticsearch.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yinxin.elasticsearch.entity.Sku;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/3010:52
 */
@Mapper
public interface SkuDao extends BaseMapper<Sku> {
}
