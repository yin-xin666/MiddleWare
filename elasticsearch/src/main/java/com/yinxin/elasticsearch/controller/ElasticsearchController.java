package com.yinxin.elasticsearch.controller;

import com.yinxin.elasticsearch.dao.SkuDao;
import com.yinxin.elasticsearch.entity.SkuInfo;
import com.yinxin.elasticsearch.service.IElasticService;
import com.yinxin.elasticsearch.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class ElasticsearchController {

    @Autowired
    IElasticService elasticService;



    @GetMapping(value = "/importData",name = "并添加数据")
    public boolean importData(){
        boolean flag = elasticService.importData();
        return flag;
    }
    @Autowired
    SearchService searchService;

    @PostMapping("/search")
    @ResponseBody
    public Map search(@RequestParam Map<String,String> searchMap){
//        handleSearchMap(searchMap);
        Map map = searchService.search(searchMap);
        return map;
    }

    //参数值 key:"spec_"开头的 value: + ----> %2B 处理入参的方法
    private void handleSearchMap(Map<String,String>searchMap){
        Set<Map.Entry<String, String>> entries = searchMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            if(entry.getKey().startsWith("spec_")){
                String replace = entry.getValue().replace("+", "%2B");
                searchMap.put(entry.getKey(), replace);
            }
        }
    }

    @GetMapping(value = "/add", name = "初始化索引")
    public void add() {
        elasticService.createIndex();
        List<SkuInfo> list = new ArrayList<>();
        list.add(new SkuInfo());
        elasticService.saveAll(list);
    }


    @GetMapping(value = "/all", name = "查询所有数据")
    public List<SkuInfo> all() {
        Iterator<SkuInfo> iterators = elasticService.findAll();
        List<SkuInfo> skuInfoList = new ArrayList<>();
        while (iterators.hasNext()) {
            SkuInfo nextDocBean = iterators.next();
            skuInfoList.add(nextDocBean);
        }
        return skuInfoList;
    }

    @GetMapping(value = "/findById", name = "根据id查询数据")
    public SkuInfo findById(Long id) {
        SkuInfo skuInfo = elasticService.findById(id);
        return skuInfo;
    }

    @PostMapping(value = "/saveAll", name = "删除索引")
    public String saveAll(@RequestBody List<SkuInfo> skuInfoList) {
        elasticService.saveAll(skuInfoList);
        return "saveAll success!!!!";
    }


//    ==================操作索引==================

    @GetMapping(value = "/delete", name = "删除索引")
    public String delete() {
        elasticService.deleteIndex("ems");
        return "delete index success!!!!";
    }

    @GetMapping(value = "/create", name = "创建索引")
    public String create() {
        elasticService.createIndex();
        return "create index success!!!!";
    }
}
