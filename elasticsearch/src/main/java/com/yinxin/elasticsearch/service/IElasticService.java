package com.yinxin.elasticsearch.service;


import com.yinxin.elasticsearch.entity.SkuInfo;
import org.springframework.data.domain.Page;

import java.util.Iterator;
import java.util.List;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/1216:02
 */
public interface IElasticService {
    void createIndex();

    void deleteIndex(String index);

    void save(SkuInfo docBean);

    void isExist();

    void saveAll(List<SkuInfo> list);

    Iterator<SkuInfo> findAll();

    SkuInfo findById(Long id);

    Page<SkuInfo> findByContent(String content);

    Page<SkuInfo> findByFirstCode(String firstCode);

    Page<SkuInfo> findBySecordCode(String secordCode);

    Page<SkuInfo> query(String key);

    boolean importData();

}
