package com.yinxin.elasticsearch.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yinxin.elasticsearch.dao.ElasticRepository;
import com.yinxin.elasticsearch.dao.SkuDao;
import com.yinxin.elasticsearch.dao.SpuDao;
import com.yinxin.elasticsearch.entity.Sku;
import com.yinxin.elasticsearch.entity.SkuInfo;
import com.yinxin.elasticsearch.entity.Spu;
import com.yinxin.elasticsearch.service.IElasticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.AliasQuery;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/1216:03
 */
@Service
public class ElasticServiceImpl implements IElasticService {

    @Autowired
    SkuDao skuDao;

    @Autowired
    SpuDao spuDao;

    @Autowired
    ElasticRepository elasticRepository;

    @Override
    public boolean importData() {
        boolean flag =false;

        LambdaQueryWrapper<Spu> spuWrapper = new LambdaQueryWrapper<>();
        spuWrapper.eq(Spu::getIsMarketable,0);
        List<Spu> spus = spuDao.selectList(spuWrapper);
        for (Spu spu : spus) {
            LambdaQueryWrapper<Sku> skuWrapper = new LambdaQueryWrapper<>();
            skuWrapper.eq(Sku::getSpuId,spu.getId());
            List<Sku> skus = skuDao.selectList(skuWrapper);
            List<SkuInfo> skuinfoList = new ArrayList<>();
            for (Sku sku : skus) {
                String skuStr = JSON.toJSONString(sku);
                SkuInfo skuInfo = JSON.parseObject(skuStr, SkuInfo.class);
                //需求将 Sku.spec String --->  SkuInfo.specMap Map
                Map map = JSON.parseObject(sku.getSpec(), Map.class);
                skuInfo.setSpecMap(map);
                skuinfoList.add(skuInfo);
            }
            if(skuinfoList.size()>0){
                flag=true;
            }
            elasticRepository.saveAll(skuinfoList);
        }
        return flag;
    }

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;


    public static String esIndex = "skuinfo";

    @Override
    public void createIndex() {
//        elasticsearchRestTemplate.indexOps(IndexCoordinates.of(esIndex)).create();
        IndexOperations indexOperations = elasticsearchRestTemplate.indexOps(IndexCoordinates.of(esIndex));
        // 不存在这个索引就创建
        if(!indexOperations.exists()){
            indexOperations.create();
        }
    }

    @Override
    public void deleteIndex(String index) {
        elasticsearchRestTemplate.indexOps(IndexCoordinates.of(esIndex)).delete();
    }

    @Override
    public void save(SkuInfo docBean) {
        elasticRepository.save(docBean);
    }

    @Override
    public void isExist() {
        elasticsearchRestTemplate.indexOps(IndexCoordinates.of(esIndex)).exists();
    }

    @Override
    public void saveAll(List<SkuInfo> list) {
        elasticRepository.saveAll(list);
    }

    @Override
    public Iterator<SkuInfo> findAll() {
        Iterable<SkuInfo> repositoryAll = elasticRepository.findAll();
        return repositoryAll.iterator();
    }

    @Override
    public SkuInfo findById(Long id) {
        Optional<SkuInfo> optional = elasticRepository.findById(id);
        SkuInfo docBean = null;
        if(optional.isPresent()){
            docBean = optional.get();
        }
        return docBean;
    }

    @Override
    public Page<SkuInfo> findByContent(String content) {
        return null;
    }

    @Override
    public Page<SkuInfo> findByFirstCode(String firstCode) {
        return null;
    }

    @Override
    public Page<SkuInfo> findBySecordCode(String secordCode) {
        return null;
    }

    @Override
    public Page<SkuInfo> query(String key) {
        return null;
    }
}
