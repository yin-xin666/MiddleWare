package com.yinxin.elasticsearch.service;

import java.util.Map;

/**
 * @author yinxin
 * @version 1.0
 * @Description:
 * @date 2022/4/3012:05
 */
public interface SearchService {
    Map search(Map<String, String> searchMap);
}
